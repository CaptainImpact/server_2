import 'package:shelf/shelf.dart' as shelf;

class Calculator {
  static shelf.Response calculate(Map<String, dynamic> params) {
    final operation = params['operation'] as String;
    final a = params['a'] as double;
    final b = params['b'] as double;
    switch (operation) {
      case '+':
        return shelf.Response.ok((a + b).toString());
      case '-':
        return shelf.Response.ok((a - b).toString());
      case '/':
        return shelf.Response.ok((a / b).toString());
      case '*':
        return shelf.Response.ok((a * b).toString());
      default:
    return shelf.Response.forbidden('Unknown operation \"$operation\". Use one of next operators: +-/*');
    }
  }
}