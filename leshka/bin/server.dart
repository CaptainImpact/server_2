import 'dart:convert';
import 'dart:io';

import 'package:args/args.dart';
import 'package:shelf/shelf.dart' as shelf;
import 'package:shelf/shelf_io.dart' as io;

import 'calculator_functions/calculate.dart';
import 'kfc_parser/parser.dart';

// For Google Cloud Run, set _hostname to '0.0.0.0'.
const _hostname = '167.71.80.229';

void main(List<String> args) async {
  var parser = ArgParser()..addOption('port', abbr: 'p');
  var result = parser.parse(args);

  // For Google Cloud Run, we respect the PORT environment variable
  var portStr = result['port'] ?? Platform.environment['PORT'] ?? '8010';
  var port = int.tryParse(portStr);

  if (port == null) {
    stdout.writeln('Could not parse port value "$portStr" into a number.');
    // 64: command line usage error
    exitCode = 64;
    return;
  }

  var handler = const shelf.Pipeline()
      .addMiddleware(shelf.logRequests())
      .addHandler(_echoRequest);

  var server = await io.serve(handler, _hostname, port);
  print('Serving at http://${server.address.host}:${server.port}');
}

Future<shelf.Response> _echoRequest(shelf.Request request) async {
  await KFCParser.f();
  final resultString = await request.readAsString();
  final body = jsonDecode(resultString);
  switch (request.url.path){
    case '/calculate':
      return Calculator.calculate(body);
    case '/parse':
      await KFCParser.f();
      break;
    default:
      return shelf.Response.forbidden('unknown function');

  }
}
