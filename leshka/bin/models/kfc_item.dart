class KFCItem{
  final String imageUrl;
  final String title;
  final String description;

  KFCItem(this.imageUrl, this.title, this.description);
}